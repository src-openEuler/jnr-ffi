Name:                jnr-ffi
Version:             2.2.16
Release:             1
Summary:             Java Abstracted Foreign Function Layer
License:             Apache-2.0
URL:                 http://github.com/jnr/%{name}/
Source0:             https://github.com/jnr/jnr-ffi/archive/refs/tags/%{name}-%{version}.tar.gz
BuildRequires:       fdupes gcc make maven-local mvn(com.github.jnr:jffi)
BuildRequires:       mvn(com.github.jnr:jffi::native:) mvn(com.github.jnr:jnr-x86asm)
BuildRequires:       mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-source-plugin) mvn(org.ow2.asm:asm)
BuildRequires:       mvn(org.ow2.asm:asm-analysis) mvn(org.ow2.asm:asm-commons)
BuildRequires:       mvn(org.ow2.asm:asm-tree) mvn(org.ow2.asm:asm-util)
BuildRequires:       mvn(org.sonatype.oss:oss-parent:pom:)
BuildRequires:       mvn(org.eclipse.aether:aether-connector-basic)
BuildRequires:       mvn(org.eclipse.aether:aether-transport-wagon)
BuildRequires:       mvn(org.apache.maven.wagon:wagon-http)
BuildRequires:       mvn(org.apache.maven.wagon:wagon-provider-api)
BuildArch:           noarch
%description
An abstracted interface to invoking native functions from java

%package javadoc
Summary:             Javadocs for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%autosetup -n %{name}-%{name}-%{version} -p1
find -name '*.jar' -o -name '*.class' -exec rm -f '{}' \;
%pom_remove_plugin ":maven-javadoc-plugin"
sed -i 's|-Werror||' libtest/GNUmakefile 

%build
%{mvn_build} -b -f -- -Dasm.version=7.0
sed -i '/jnr-a64asm/,+2d;:go;1,2!{P;N;D};N;bgo' .xmvn-reactor
sed -i '160,161d' .xmvn-reactor
sed -i '104,105d' .xmvn-reactor

%install
%mvn_install
%fdupes -s %{buildroot}%{_javadocdir}

%files -f .mfiles
%license LICENSE

%files javadoc -f .mfiles-javadoc
%license LICENSE

%changelog
* Wed Apr 24 2024 yaoxin <yao_xin001@hoperun.com> - 2.2.16-1
- Upgrade to 2.2.16

* Thu Sep 28 2023 laokz <zhangkai@iscas.ac.cn> - 2.2.0-2
- Backport riscv64 support patch from 2.2.15

* Wed Sep 27 2023 Ge Wang <wang__ge@126.com> - 2.2.0-1
- Update to version 2.2.0

* Thu May 18 2023 huajingyun <huajingyun@loongson.cn> - 2.1.8-3
- Add loongarch64 support

* Thu May 18 2023 chenchen <chen_aka_jan@163.com> - 2.1.8-2
- remove redundant macro pkg_vcmp

* Fri Jul 31 2020 Jeffery.Gao <gaojianxing@huawei.com> - 2.1.8-1
- Package init
